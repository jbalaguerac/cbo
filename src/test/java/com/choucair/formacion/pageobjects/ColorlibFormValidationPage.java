package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*; 


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class ColorlibFormValidationPage extends PageObject {
	
	//Campo Required
	@FindBy(xpath="//*[@id='req']")
	public WebElementFacade txtRequired;
	
	//Campo seleccionar deporte 1
	@FindBy(xpath="//*[@id='sport']")
	public WebElementFacade cmbSport;
	
	
	//Campo seleccionar deporte 2
	@FindBy(xpath="//*[@id='sport2']")
	public WebElementFacade cmbSport2;
	
	//Campo seleccionar URL
	@FindBy(xpath="//*[@id='url1']")
	public WebElementFacade txtUrl;
	
	//Campo seleccionar email
	@FindBy(id="email1")
	public WebElementFacade txtEmail;
	
	//Campo seleccionar password 1
	@FindBy(xpath="//*[@id='pass1']")
	public WebElementFacade txtPass1;
	
	//Campo seleccionar password 2
	@FindBy(xpath="//*[@id='pass2']")
	public WebElementFacade txtPass2;
	
	//Campo miniSize
	@FindBy(xpath="//*[@id='minsize1']")
	public WebElementFacade txtMinsize1;
	
	//Campo maxSize
	@FindBy(xpath="//*[@id='maxsize1']")
	public WebElementFacade txtMaxsize1;
	
	//Campo Number
	@FindBy(xpath="//*[@id='number2']")
	public WebElementFacade txtNumber;
	
	//Campo IP
	@FindBy(xpath="//*[@id='ip']")
	public WebElementFacade txtIp;
	
	//Campo Date
	@FindBy(xpath="//*[@id='date3']")
	public WebElementFacade txtDate;
	
	//Campo Date Earlier
	@FindBy(xpath="//*[@id='past']")
	public WebElementFacade txtDateEarlier;
	
	//Boton Validate
	@FindBy(xpath="//*[@id=\'popup-validation\']/div[14]/input")
	public WebElementFacade btnValidate;
	
	//Campos globo informativo
	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo ;
	
		
	public void Required (String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	
	public void Select_Sport (String datoPrueba) {
		cmbSport.click();
		cmbSport.selectByVisibleText(datoPrueba);
	}
	
	public void Multipleselect (String datoPrueba) {
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	
	public void Url (String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}
	
	public void Email (String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}
	
	public void Password1 (String datoPrueba) {
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(datoPrueba);
	}
	
	public void Password2 (String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}
	
	public void miniSize (String datoPrueba) {
		txtMinsize1.click();
		txtMinsize1.clear();
		txtMinsize1.sendKeys(datoPrueba);
	}
	
	public void maxSize (String datoPrueba) {
		txtMaxsize1.click();
		txtMaxsize1.clear();
		txtMaxsize1.sendKeys(datoPrueba);
	}
	
	public void Number (String datoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}
	
	public void Ip (String datoPrueba) {
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(datoPrueba);
	}
	
	public void Date (String datoPrueba) {
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(datoPrueba);
	}
	
	public void DateEarlier (String datoPrueba) {
		txtDateEarlier.click();
		txtDateEarlier.clear();
		txtDateEarlier.sendKeys(datoPrueba);
	}
	
	public void Validate () {
		btnValidate.click();
		
	}
	
	
	public void formulario_sin_errores () {
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}	
	
	public void formulario_con_errores () {
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}
	
}


